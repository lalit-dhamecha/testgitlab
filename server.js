const express = require('express');
const product =require('./product');
const app=express();
app.use(product);
app.listen(3000,'0.0.0.0', () => {
    console.log(`Server started on port 3000`);
});